<?php
// permet d'inclure la bibliothèque fpdf
require('../fpdf/fpdf.php');

// instancie un objet de type FPDF qui permet de créer le PDF
$pdf=new FPDF();
// ajoute une page
$pdf->AddPage();
// définit la police courante
$pdf->SetFont('Arial','B',16);
// affiche une image
$pdf->Image('../images/banniere.jpg', 63, 10, 90, 60);
// affiche du texte
$pdf->Cell(10,140,'Cours : ');
// affiche du texte
$pdf->Cell(10,160,'Nom : ');
$pdf->Cell(10,180,'Prenom : ');
$pdf->Cell(10,200,'Heure : ');
// Enfin, le document est terminé et envoyé au navigateur grâce à Output().
$pdf->Output();
?>
