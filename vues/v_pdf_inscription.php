<?php
    
    function creerPdfInscription($inscription){
        // permet d'inclure la bibliothèque fpdf
        require('fpdf181/fpdf.php');
        // instancie un objet de type FPDF qui permet de créer le PDF
        $pdf=new FPDF();
        // ajoute une page
        $pdf->AddPage();
        // définit la police courante
        $pdf->SetFont('Arial','B',16);
        // affiche une image
        $pdf->Cell(10,10,"Ziqmu PDF");
        $pdf->Image('images/banniere.jpg', 63, 25, 90, 60);
        // affiche du texte
        $pdf->Ln(10);
        $pdf->Cell(10,150,'Cours : ' . $inscription->cours);
        // affiche du texte
        $pdf->Ln(10);
        $pdf->Cell(10,160,'Nom : ' . $inscription->nom);
        $pdf->Ln(10);
        $pdf->Cell(10,170,'Prenom : ' . $inscription->prenom);
        // Enfin, le document est terminé et envoyé au navigateur grâce à Output().
        ob_get_clean();
        $pdf->Output();
    }