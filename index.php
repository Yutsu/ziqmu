<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/styles.css">
        <title> Ziqmuc </title>
    </head>

    <body>
        <?php
        
        if(!isset($_REQUEST['action']))
            $action = 'accueil';
        else
            $action = $_REQUEST['action'];
        
        
        if( $action == "pdfInscription"){

        }
        else{
            include ("vues/v_banniere.php");
        }
        
        
        // vue qui crée l'en-tête de la page
        //include ("vues/v_banniere.php");
        include ("modele/fonctions.php");
        
        switch ($action){
            case 'accueil':
                include ("vues/v_accueil.php");
                break;

            case 'voirCours':
                $lesCours = getLesCours();
                include ("vues/v_cours.php");                
                break;
       
            case 'inscrire':
                $numero = inscrireCours();
                include ("vues/v_formulaireInscription.php"); 
                break;
            
            case 'validerInscription':
                $inscription = validerInscription();
                $numero = $inscription['numero'];
                $nom = $inscription['nom'];
                $prénom = $inscription['prenom'];
                include("vues/v_confirmerInscription.php");
                break;
            
            case 'voirInscription':
                $lesInscriptions = getLesInscription();
                include ("vues/v_inscription.php");
                break;
            
            case 'pdfInscription':
                $lesInscriptions = getLesInscription();
                $numero = $_REQUEST['numInscription'];
                $inscription = $lesInscriptions[$numero];
                include ("vues/v_pdf_inscription.php");
                $res = creerPdfInscription($inscription);
                break;
            
            case 'supprimer':
                $suppInscris = $_REQUEST['numeroSupp'];
                $supprimer = getSupprimer($suppInscris);
                include("vues/v_supprimer.php");
                break;
        }
        
        
        
        
        if( $action == "pdfInscription"){

        }
        else{
        include ('vues/v_footer.php');
        }

      
        ?>
    </body>
</html>
