
<?php
include('fonction.php');


    function getLesCours(){
        try{
            $bdd = connection();
            $requete = 'select cours.idCours, cours.dateCours, cours.heureCours, matiere.nomMatiere from ziqmu.cours join ziqmu.matiere on cours.matiere = matiere.idMatiere';
            $resultat = $bdd->query($requete);
            $lesCours = array();

            while($cours = $resultat->fetch(PDO::FETCH_OBJ)){

               $lesCours[]=$cours ;

            }
            return($lesCours);

        } catch (Exception $e) {
            echo "Erreur dans la requête" .$e->getMessage();
        }
    }
    
    function inscrireCours(){
        //Récupère le numéro du cours
        $numero = $_REQUEST['numero'];
        return $numero;
    }
    
    function validerInscription(){
        $inscription = array();
            //recuperation du numéro
        $numero = $_REQUEST["numero"];
        $inscription["numero"] = $numero;

        $nom = $_REQUEST["nom"];
        $inscription["nom"] = $nom ;

        $prénom = $_REQUEST["prenom"];
        $inscription["prenom"] = $prénom;

        $mail = $_REQUEST["mail"];
        $inscription["mail"] = $mail;

        $telephone = $_REQUEST["telephone"];
        $inscription["telephone"] = $telephone;


                
        initPanier();
      ajouterAuPanier($inscription);
      creerInscription($inscription);
                
        return $inscription;
    }
    
    function initPanier(){
        if(!isset($_SESSION['inscriptions'])) {
        $_SESSION['inscriptions'] = array();
    }
}
    
    function ajouterAuPanier($inscription) {
        $_SESSION['inscriptions'][]= $inscription;
    }

    function creerInscription($inscription){
        try{
            $bdd = connection();
            $requete = "insert into ziqmu.adherent (nom, prenom, mail, tel) VALUES('".htmlspecialchars($inscription['nom'])."','".htmlspecialchars($inscription['prenom'])."','".htmlspecialchars($inscription['mail'])."','".htmlspecialchars($inscription['telephone'])."')";
            //echo $requete ;
            $bdd->query($requete);

            $requete2 = "select MAX(idAdherent) FROM ziqmu.adherent";
            $resultat2 = $bdd->query($requete2);
            
            echo"\n";
            $adherentInscript = $resultat2->fetch();

            $requete3 = "insert into ziqmu.inscription (cours, adherent) VALUES(($inscription[numero]), $adherentInscript[0])";
            $bdd->query($requete3);

        } catch (Exception $e) {
            echo "Erreur dans la requête" .$e->getMessage();
        }
        return $inscription;
    }
    

    function getLesInscription(){
        try{
            $bdd = connection();
            $requete = 'select inscription.cours, adherent.idAdherent, adherent.nom, adherent.prenom from ziqmu.inscription join ziqmu.adherent on inscription.adherent = adherent.idAdherent';
            $resultat = $bdd->query($requete);
            $lesInscriptions = array();

            while($inscrit = $resultat->fetch(PDO::FETCH_OBJ)){

               $lesInscriptions[]= $inscrit;

            }
            return($lesInscriptions);

        } catch (Exception $e) {
            echo "Erreur dans la requête" .$e->getMessage();
        }
    }
    
    /*
    function getInscription($numInscription){
        try{
            $sql = $_SESSION['bdd']->prepare("select inscription.cours, adherent.nom, adherent.prenom from inscription join adherent on inscription.adherent = adherent.idAdherent");
            $sql->execute();
            $uneInscription = array();
            
            while($inscris = $sql->fetch(PDO::FETCH_OBJ)){      
                array_push($uneInscription, $inscris);
            }
            
            $_SESSION['inscription'] = $uneInscription[$numInscription-1];
        
        } catch (Exception $e) {
            echo "Erreur dans la requête" .$e->getMessage();
        }
        return $uneInscription;
    }
     
     */
    
    function getSupprimer($suppInscris){
         try{
            $bdd = connection();
            $requete = "delete * from ziqmu.adherent where adherent.idAdherent=?";
            $resultat = $bdd->query($requete);
            return($resultat);

        } catch (Exception $e) {
            echo "Erreur dans la requête" .$e->getMessage();
        }
    }
